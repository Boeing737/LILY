Survey.Survey.cssType = "bootstrap";
Survey.defaultBootstrapCss.navigationButton = "btn btn-green";

window.survey = new Survey.Model({ questions: [
  { type: "matrix", name: "Lecture_Quality", title: "오늘의 강의에 대해 평가해주세요.",isRequired: true,
              columns: [{ value: 1, text: "매우 불만족" },
                        { value: 2, text: "불만족" },
                        { value: 3, text: "보통" },
                        { value: 4, text: "만족" },
                        { value: 5, text: "매우 만족" }],
              rows: [{ value: "contents", text: "강의 컨텐츠는 적절했나요?" },
                     { value: "difficulty", text: "강의 난이도는 적절했나요?" },
                     { value: "duration", text: "강의 시간은 적절했나요?" },
                     { value: "preparation", text: "강사의 준비성은 적절했나요?" },
                     { value: "speed", text: "강사의 강의 속도는 적절했나요?" }
                    ]},
  { type: "matrix", name: "Lecturer_Quality", title: "LILY - 이 프로그램에 대해 평가해주세요.",isRequired: true,
              columns: [{ value: 1, text: "매우 불만족" },
                        { value: 2, text: "불만족" },
                        { value: 3, text: "보통" },
                        { value: 4, text: "만족" },
                        { value: 5, text: "매우 만족" }],
              rows: [{ value: "preparation", text: "전체적인 디자인은 괜찮으셨나요?" },
                     { value: "speed", text: "프로그램의 안정성은 어떠셨나요?" }]},
  {type:"comment",name:"오늘 강의에 대해, 혹은 이 프로그램에 대해 하고싶은 말이 있다면 적어주세요.",isRequired: true}

]});
survey.onComplete.add(function(result) {
	document.querySelector('#surveyResult').innerHTML = "result: " + JSON.stringify(result.data);
  document.querySelector('#surveyResult').innerHTML = "<hr>";
  document.querySelector('#surveyResult').innerHTML = "<h4>설문이 완료되었습니다.</h4>";
});


$("#surveyElement").Survey({model:survey});
