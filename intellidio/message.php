<?php
Header('Content-Type: application/json; charset=utf-8');
// 요청을 받아 저장
$data = json_decode(file_get_contents('php://input'), true);

// 받은 요청에서 content 항목 설정
$content = $data["content"];


//언어 추천
if( strpos($content, "언어") && strpos($content, "추천")){
  echo <<< EOD
  {
    "message": {
      "text": "언어를 추천해 드리겠습니다. 아래 항목 중 자신에게 알맞은 것을 선택해 주세요."
    },
    "keyboard": {
     "type": "buttons",
     "buttons": [
       "돈을 벌고 싶어요.",
       "어린아이를 가르쳐주고싶어요.",
       "딱히 이유는 없고 한번 배우고 싶어요.",
       "그냥 재미있어보여요.",
       "자기개발을 위해서요.",
       "그만할래요"
     ]
    }
  }
EOD;
}
else if($content == "돈을 벌고 싶어요."){
  echo <<< EOD
  {
    "message": {
      "text": "취직을 하고 싶은가요? 창업을 하고 싶은가요?"
    },
    "keyboard": {
     "type": "buttons",
     "buttons": [
       "취직을 하고 싶어요.",
       "창업을 하고 싶어요.",
       "그만할래요"
     ]
    }
  }
EOD;
}
else if($content == "창업을 하고 싶어요."){
  echo <<< EOD
  {
    "message": {
      "text": "창업을 하기위한 아이디어가 있나요?"
    },
    "keyboard": {
     "type": "buttons",
     "buttons": [
       "창업하려는 아이디어가 있어요.",
       "아직 아이디어는 없어요.",
       "그만할래요"
     ]
    }
  }
EOD;
}
else if($content == "아직 아이디어는 없어요." || $content == "그냥 재미있어보여요."){
  echo <<< EOD
  {
    "message": {
      "text": "프로그래밍 언어를 배울 때 난이도는 어땠으면 좋겠나요?"
    },
    "keyboard": {
     "type": "buttons",
     "buttons": [
       "쉽고 빠르게 배우고 싶어요.",
       "조금 어렵지만 빨리 배우고 싶어요.",
       "깊게 배우고 싶어요.",
       "어려워도 제대로 해보고 싶어요.",
       "그만할래요"
     ]
    }
  }
EOD;
}
else if($content == "취직을 하고 싶어요."){
  echo <<< EOD
  {
    "message": {
      "text": "취직을 해서 일을 하고 싶은 분야가 있나요?"
    },
    "keyboard": {
     "type": "buttons",
     "buttons": [
       "하고싶은 분야가 있어요.",
       "딱히 하고싶은 분야는 없어요.",
       "그만할래요"
     ]
    }
  }
EOD;
}
else if($content == "딱히 하고싶은 분야는 없어요."){
  echo <<< EOD
  {
    "message": {
      "text": "돈을 잘 벌고 싶은가요? 큰 기업으로 가고 싶은가요?"
    },
    "keyboard": {
     "type": "buttons",
     "buttons": [
       "돈을 많이 받고 싶어요.",
       "큰 기업으로 가고 싶어요.",
       "그만할래요"
     ]
    }
  }
EOD;
}
else if($content == "큰 기업으로 가고 싶어요."){
  echo <<< EOD
  {
    "message": {
      "text": "해외 유명 대기업 중에 어떤 곳을 선호하나요?"
    },
    "keyboard": {
     "type": "buttons",
     "buttons": [
       "페이스북",
       "구글",
       "마이크로소프트",
       "애플",
       "그만할래요"
     ]
    }
  }
EOD;
}
else if($content == "하고싶은 분야가 있어요." || $content == "창업하려는 아이디어가 있어요." || $content == "자기개발을 위해서요."){
  echo <<< EOD
  {
    "message": {
      "text": "어떠한 분야로 개발을 하고 싶나요?"
    },
    "keyboard": {
     "type": "buttons",
     "buttons": [
       "웹",
       "게임",
       "어플/모바일",
       "그만할래요"
     ]
    }
  }
EOD;
}
else if($content == "어플/모바일"){
  echo <<< EOD
  {
    "message": {
      "text": "안드로이드와 아이폰 중 어떤 어플을 개발하고 싶나요?"
    },
    "keyboard": {
     "type": "buttons",
     "buttons": [
       "안드로이드",
       "아이폰",
       "그만할래요"
     ]
    }
  }
EOD;
}
else if($content == "웹"){
  echo <<< EOD
  {
    "message": {
      "text": "디자인 위주의 프론트엔드와 서버 및 데이터베이스에 대한 백엔드 중 어떤 것을 선호하나요?"
    },
    "keyboard": {
     "type": "buttons",
     "buttons": [
       "프론트엔드(UI/UX)",
       "백엔드(서버)",
       "그만할래요"
     ]
    }
  }
EOD;
}
else if($content == "백엔드(서버)"){
  echo <<< EOD
  {
    "message": {
      "text": "장난감을 가지고 논다면 어떤 장난감을 선호하나요?"
    },
    "keyboard": {
     "type": "buttons",
     "buttons": [
       "레고",
       "모래놀이",
       "익숙한 장난감",
       "그만할래요"
     ]
    }
  }
EOD;
}
else if($content == "그만할래요"){
  echo <<< EOD
  {
    "message": {
      "text": "그럼 같이 놀자~"
    }
  }
EOD;
}
//최종 답변
else if($content ==  "어린아이를 가르쳐주고싶어요."){
  echo <<< EOD
  {
    "message": {
      "text": "Scratch(스크래치)를 한번 배워보면 어떨까요?"
    }
  }
EOD;
}
else if($content == "돈을 많이 받고 싶어요." || $content == "안드로이드" || $content == "조금 어렵지만 빨리 배우고 싶어요."){
  echo <<< EOD
  {
    "message": {
      "text": "Java(자바)를 한번 배워보면 어떨까요?"
    }
  }
EOD;
}
else if($content == "딱히 이유는 없고 한번 배우고 싶어요." || $content == "페이스북" || $content == "구글" || $content == "레고" || $content == "쉽고 빠르게 배우고 싶어요."){
  echo <<< EOD
  {
    "message": {
      "text": "Phython(파이썬)을 한번 배워보면 어떨까요?"
    }
  }
EOD;
}
else if($content == "게임" || $content == "어려워도 제대로 해보고 싶어요."){
  echo <<< EOD
  {
    "message": {
      "text": "C++(씨쁠쁠)을 한번 배워보면 어떨까요?"
    }
  }
EOD;
}
else if($content == "아이폰" || $content == "깊게 배우고 싶어요."){
  echo <<< EOD
  {
    "message": {
      "text": "C(씨언어)를 한번 배워보면 어떨까요?"
    }
  }
EOD;
}
else if($content == "프론트엔드(UI/UX)"){
  echo <<< EOD
  {
    "message": {
      "text": "Javascript(자바스크립트)를 한번 배워보면 어떨까요?"
    }
  }
EOD;
}
else if($content == "모래놀이"){
  echo <<< EOD
  {
    "message": {
      "text": "Ruby(루비)를 한번 배워보면 어떨까요?"
    }
  }
EOD;
}
else if($content == "익숙한 장난감"){
  echo <<< EOD
  {
    "message": {
      "text": "PHP(피에이치피)를 한번 배워보면 어떨까요?"
    }
  }
EOD;
}
// '시작하기' 버튼 처리
if( $content == "시작하기" )
{
echo <<< EOD
{
  "message": {
    "text": "Intellidio에게 질문을 해보세요."
  }
}
EOD;
}
// '도움말' 버튼 처리
else if( $content == "설명")
{
echo <<< EOD
{
  "message": {
    "text": "Intellidio 챗봇입니다."
  }
}
EOD;
}

// '안녕'이란 단어가 포함되었을때 처리
else if( strpos($content, "안녕") !== false )
{
echo <<< EOD
{
  "message": {
    "text": "안녕~~ 반가워 ㅎㅎ"
  }
}
EOD;
}
// 그밖의 문장일때
else
{
echo <<< EOD
{
  "message": {
    "text": "모르겠다."
  }
}
EOD;
}
?>
