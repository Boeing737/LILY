<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, height=device-height" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="./SelectRoom.css?version=5">
<link rel="stylesheet" href="./StudentPage.css?version=9">
<link rel="stylesheet" href="./Lecturer.css?version=8">
<link rel="shortcut icon" href="../img/title_logo.png" type="image/png">
<title>LILY</title>
</head>
<body>
  <?php $user_name = $_POST['user_Name'];?>
  <?php $user_id = $_POST['user_Id'];?>
  <input type="hidden" id="user_id" value="<?php echo $user_id?>">
	<div id="greetings" class="greetingContainer">

		<img class="arrow" onclick="location.href='../index.html'"
			src="../img/arrow.png" alt="arrow">
		<div class="greetingText">
			<span id="user_Name"><?php echo $user_name?></span>
			님 안녕하세요.
		</div>
		<img onclick="create_room('<?php echo $user_id?>')" class="CreateRoomButton" src="../img/cross.png" alt="">

	</div>
	<br>
	<div id="SlideUp">
		<div class="EnterTheRoom">
      <div class="inputHeading">
        CLASS NO.
      </div>
      <input id = "RoomCode" class="inputRoomCode" type="text" value=""
				onkeyup="Room_enter(event, '<?php echo $user_id?>')"
			 	placeholder="Room Code를 입력하세요.">
		</div>
      <a onclick="enter_room('<?php echo $user_id?>')">ENTER</a>

	</div>

  <div id="newSlideUp">
    <div class="lecturer_heading_container">
      <div class="Lecturer_text">Student</div>
      <div class="right_heading">
        <span class="Room_Code"></span>
        <div class="StudentCount">
          <img src="../img/how_many_icon.png" alt="">
          <div class="nav_text">
            <span class="Student_Count"></span>명 접속 중
          </div>
        </div>
      </div>
      <!-- <button type="button" name="button" onclick="Student_Enter();">학생입장</button> -->

      <!-- <button id="Open_Chat" type="button" name="button"><img src="../img/chat_normal.png" id="chat_img"></button> -->
    </div>
		<div class="studentContainer">
      <button id="survey" class="survey_button" type="button" name="button"><i class="fa fa-list-ul" aria-hidden="true"></i> 설문 시작</button>
			<a class="room_out_button" onclick="out_room('<?php echo $user_id?>')"><img src="../img/out_button.png" alt=""></a>
			<div class="check_button_container">
				<img class="question_button" onclick="question_btn('<?php echo $user_id ?>');" src="../img/question_button.png" alt="">
				<img class="ok_button" onclick="complete_btn('<?php echo $user_id ?>');" src="../img/ok_button.png" alt="">
			</div>

      <div id="questionContainer"></div>
      <!-- <div class="inputContainer">
				<input type="text" id="user_message" value=""
					placeholder="여기에 입력하세요."
					onkeyup="Student_enter(event, '<?php echo $user_id?>')">
				<a onclick="input_question('<?php echo $user_id?>');";>Send</a>
			</div> -->
		</div>
    <table class="inputContainer">
      <tr>
        <td class="input_text_container"><input type="text" id="user_message" value=""
          placeholder="여기에 입력하세요."
          onkeyup="Student_enter(event, '<?php echo $user_id?>')">
        </td>
        <td class="send_button_container">
          <a onclick="input_question('<?php echo $user_id?>');";><span>Send</span></a>
        </td>
      </tr>
    </table>
	</div>
  <div id="survey_frame" class="survey_frame">
    <button id="close_survey" type="button" name="button">설문 닫기</button>
    <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSev-kvzcLibOKwEvPBwgM6qmMCjt5gOWdz_nvjE14xtJ1UVDw/viewform?fbzx=-354315011964748700">
    </iframe>
  </div>

	<div id="Lecturer">
    <div id="Lecturer_contents">
      <nav>
          <!-- <button onclick="">나가기</button> -->
        <div class="lecturer_heading_container">
          <div class="Lecturer_text">Lecturer</div>
          <div class="right_heading">
            <span class="Room_Code"></span>
            <div class="StudentCount">
              <img src="../img/how_many_icon.png" alt="">
              <div class="nav_text">
                <span class="Student_Count"></span>명 접속 중
              </div>
            </div>
          </div>
          <!-- <button type="button" name="button" onclick="Student_Enter();">학생입장</button> -->

          <!-- <button id="Open_Chat" type="button" name="button"><img src="../img/chat_normal.png" id="chat_img"></button> -->
        </div>
      </nav>
  		<div class="StudentListContainer">
  			<div class="UpperContainer">
  				<div id="StudentList">
          </div>
          <!-- <div id="shadow" class="shadow"></div> -->
  			</div>
        <div class="lecturerButtonContainer">
  				<button class="Lecture_button out_button" name="button" onclick="out_room('<?php echo $user_id?>')">나가기</button>
  				<button class="Lecture_button send_button" type="button" name="button" onclick="openModal('전체');">링크 전송</button>
          <button class="Lecture_button" type="button" name="button" onclick="send_survey();">설문 조사</button>
  				<div class="nextButton">
  					<span class="nextText">다음으로 진행하기</span>
            <img id="nextSessionButton" src="../img/arrow.png" alt="left_arrow" onclick="reset_student_data()">
  				</div>
  			</div>
  		</div>
    </div>
    <div id="lecturer_chat_container">
      <div id="Open_Chat">
        <img src="../img/chat_arrow.png" id="chat_arrow" onclick="" alt="">
      </div>

      <div id="LecturerChat" class="LecturerSlideChat">
        <div id="contents">
          <div id="Chat_Area"></div>
          <!-- <input id="myMessage" type="text" name="" value=""
            onkeyup="Lecture_enter(event)">
          <button id="sendButton" type="button" name="button"
            onclick="lecture_send_btn()">전송</button> -->
        </div>
      </div>
    </div>
	</div>
	<div class="Modal">
		<table class="modal_table">
			<thead>
				<th></th>
				<th><span id="send_name">전체</span> 전송</th>
				<th id="closeButton" onclick="closeModal();">x</th>
			</thead>
			<tbody>
				<tr></tr>
				<tr>
					<td id="blank"></td>
					<td colspan="2" id="line"></td>
				</tr>
				<tr>
					<td colspan="3"><input type="text" name="" value=""
						id="input_link" placeholder="링크를 입력하세요."></td>
				</tr>
				<tr>
					<td colspan="3"><button type="button" name="button"
							class="Modal_btn" onclick="send_link('전체')">링크전송</button></td>
				</tr>
				<!-- <tr>
					<td colspan="3"><input type="text" name="" value=""
						id="input_file" placeholder="파일 경로"></td>
				</tr>
				<tr>
					<td colspan="3"><button type="button" name="button"
							class="Modal_btn" id="add_file">파일 첨부</button>
						<button type="button" name="button" class="Modal_btn"
							id="send_file">파일 전송</button></td>
				</tr> -->
				<tr></tr>
			</tbody>
		</table>
	</div>
  <script type="text/javascript" src="survey.js?version=2"
    charset="UTF-8"></script>
  <script type="text/javascript" src="SelectRoom.js?version=3"
  	charset="UTF-8"></script>
  <!-- <script type="text/javascript" src="mousewheel.js?version=1"></script> -->
</body>
</html>
