var webSocket = new WebSocket('ws://13.124.150.80:8080/LILY/WebSocket');

var is_lecture = 1;
var createdId_Index = 1;
var save_room_code = "";
var time;

window.onload = function() {
	var user_name = document.getElementById("user_Name");
	if (user_name.innerHTML == "null" || user_name.innerHTML === "") {
		alert("로그인이 필요합니다.");
		location.href = "../index.html";
	}
	slide_Down();
	slide_Up();
}

webSocket.onopen = function(event) {

}

webSocket.onmessage = function(event) {
	var response = JSON.parse(event.data);
	var room_code = save_room_code;
	console.log(response);
	if (room_code !== response.room_code) {
		return;
	}
	if (response.to === "message") {
		if (is_lecture === 1) {
			//Other_Message(response.msg);
			Other_Message("0",response.msg);
			var show_chat_img = document.getElementById("chat_img");
			//show_chat_img.src = "../img/chat_alert.png";
		} else {
			if (response.user_id === "lecture") {
				Lecture_Message(response.msg);
			} else {
				// Student_Other_Message(response.msg);
				question("0", response.msg);
			}
		}
	} else if (response.to === "people") {
		document.getElementsByClassName("Student_Count")[is_lecture].innerHTML = response.Student_Count;
		if (is_lecture === 1 && response.state == "new_person") {
			add_student_data(response.user_id, response.user_name, 0, 0, 0);
		}
	} else if (is_lecture === 1 && response.to === "complete") {
		var student = document.getElementById(response.user_id);
		var img_Container = student.getElementsByClassName("alertMark")[0];
		student.lastChild.style.backgroundColor = "#59CC98";
		// img_Container.src = "../img/check.png";
		// img_Container.style.display = "block";
	} else if (is_lecture === 1 && response.to === "question") {
		var student = document.getElementById(response.user_id);
		var img_Container = student.getElementsByClassName("alertMark")[0];
		student.lastChild.style.backgroundColor = "#E85280";
		//img_Container.src = "../img/exclamation.png";
		// img_Container.style.display = "block";
	} else if (response.to === "url") {
		if (response.name === "전체") {
			open_url(response.url);
		} else if (response.name === document.getElementById("user_Name").innerHTML) {
			open_url(response.url);
		}
	}
	//else if(response.to === "google_survey"){open_url(response.url);}
	else if(response.to === "lily_survey"){
		var survey = document.getElementsByClassName('survey_button')[0];
	  survey.style.display = "inline-block";
	} else if (response.to === "out") {
      if(is_lecture === 1){
            document.getElementById(response.user_id).remove();
      }
      document.getElementsByClassName("Student_Count")[is_lecture].innerHTML = response.Student_Count;
   } else if (response.to === 'like'){
		var like_div = document.getElementsByClassName("like_count")[response.like_index];
		like_div.innerHTML = response.likes;
		sorting(response.like_index);
	}
};

var open_url = function(url) {
	if (confirm(url + "로 이동하시겠습니까?")) {
		window.open(url, "");
	}
}
var student_contents = document.getElementById("newSlideUp");
var survey_contents = document.getElementById('survey_frame');

var survey_opened = 0;

$('.survey_button').click(function(){
	console.log('yes');
	student_contents.style.transition = "500ms";
	student_contents.style.filter = "brightness(60%)";
	survey_contents.style.display = "block";
	survey_opened = 1;
});
$('#close_survey').click(function(){
	student_contents.style.filter = "brightness(100%)";
	survey_contents.style.display = "none";
	survey_opened = 0;
});

var create_room = function(user_id) {
	var user_name = document.getElementById("user_Name").innerHTML;

	$.ajax({
				type : "POST",
				url : "CreateRoom.php",
				dataType : "json",
				data : {
					"user_id" : user_id
				},
				success : function(response) {
					console.log("yers");
					if (response.error === "error") {
						alert("방 생성에 실패하였습니다.");
					} else if (response.error === "exist") {
						alert("이미" + response.roomcode+ " 방에 참여중입니다.");
					} else {
						alert(response.roomcode + "방이 생성되었습니다.");
						is_lecture = 1;
						save_room_code = response.roomcode+"";
						get_Student_Count(user_id, user_name, save_room_code,
								is_lecture);
						document.getElementsByClassName("Room_Code")[is_lecture].innerHTML = save_room_code;
						document.getElementsByClassName("Student_Count")[is_lecture].innerHTML = "1";
						bringLecturerPage();
						hidePage();
					}
				}
			});
}

var Room_enter = function(event, user_id) {
	if (event.key == "Enter") {
		enter_room(user_id);
	}
}

var enter_room = function(user_id) {
	var room_code = document.getElementById("RoomCode");
	console.log(room_code);
	var user_name = document.getElementById("user_Name").innerHTML;
	console.log(user_id);
	$.ajax({
				type : "POST",
				url : "./EnterRoom.php",
				data : {
					"room_code" : room_code.value,
					"user_id" : user_id
				},
				success : function(data) {
					check_data = $.trim(data);
					console.log(check_data);
					if (check_data === "ERROR") {
						alert("존재하지 않는 Room Code입니다.");
					} else if (check_data === "NOT_ERROR") {
						alert(document.getElementById("RoomCode").value + "입장");
						is_lecture = 0;
						save_room_code = room_code.value;
						get_Student_Count(user_id, user_name, save_room_code,
								is_lecture);
						document.getElementsByClassName("Room_Code")[is_lecture].innerHTML = save_room_code;
						Student_Load_Chat(user_id);
						EnterRoom();
						hidePage();
						room_code.value = "";
					} else if (check_data === "LECTURE") {
						alert("강사입장");
						is_lecture = 1;
						save_room_code = room_code.value;
						get_Student_Count(user_id, user_name, save_room_code,
								is_lecture);
						get_Student_Data(room_code.value);
						document.getElementsByClassName("Room_Code")[is_lecture].innerHTML = save_room_code;
						Lecture_Load_Chat(user_id);
						bringLecturerPage();
						hidePage();
						room_code.value = "";

					} else {
						alert("이미" + check_data + " 방에 참여중입니다.");
					}
				}
			});
}

var out_room = function(user_id) {
	var user_name = document.getElementById("user_Name").innerHTML;
	// get_Student_Count(user_id, user_name, save_room_code, is_lecture);
	$.ajax({
				type : "POST",
				url : "./get_out_Student.php",
				dataType : "json",
				data : {
					"room_code" : save_room_code,
					"user_id" : user_id
				},
				success : function(response) {
					console.log(response);
               response = $.trim(response);
               webSocket.send(JSON.stringify({
                  to : "out",
                  Student_Count : response,
                  user_id : user_id,
                  room_code : save_room_code
               }));
               if(is_lecture === 1){
								 var va = document.getElementById("Chat_Area");
								 var va_length = va.childNodes.length;
								 for(var i = 0; i<va_length; i++){
									 va.childNodes[0].remove();
								 }
								 var va = document.getElementById("StudentList");
								 var va_length = va.childNodes.length;
								 for(var i = 0; i<va_length; i++){
									 va.childNodes[0].remove();
								 }
                  get_out_LecturePage();
               }else{
								 var va = document.getElementById("questionContainer");
								 var va_length = va.childNodes.length;
								 for(var i = 0; i<va_length; i++){
									 va.childNodes[0].remove();
								 }
                  get_out_StudentPage();
               }
				}
			});
}


// lecture == 1 => student, lecture == 0 =>lecture
// enter == "1" => enter, enter == "0" => out
var get_Student_Count = function(user_id, user_name, room_code, lecture) {
	$.ajax({
				type : "POST",
				url : "./get_Student_Count.php",
				dataType : "json",
				data : {
					"room_code" : room_code,
					"user_id" : user_id
				},
				success : function(response) {
					console.log(response);
					document.getElementsByClassName("Student_Count")[lecture].innerHTML = response.Student_Count;
					webSocket.send(JSON.stringify({
						to : "people",
						Student_Count : response.Student_Count,
						state : response.state,
						user_id : user_id,
						user_name : user_name,
						room_code : room_code
					}));
				}
			});
}

var get_Student_Data = function(room_code) {
	$.ajax({
		type : "POST",
		url : "./get_Student_Data.php",
		dataType : "json",
		data : {
			"room_code" : room_code
		},
		success : function(response) {
			console.log(response);
			$.each(response, function(index, real_data) {
				add_student_data(real_data.user_id, real_data.user_name,
						real_data.question_count, real_data.pass_count,
						real_data.state);
			});
		}
	});
}

var Student_Load_Chat = function(user_id) {
	$.ajax({
		type : "POST",
		url : "./Load_Chat.php",
		dataType : "json",
		data : {
			"room_code" : save_room_code
		},
		success : function(response) {
			console.log(response);
			$.each(response, function(index, real_data) {
				// Lecture_Message Student_Other_Message Student_My_Message
				question(real_data.likes, real_data.contents);
				sorting(document.getElementsByClassName('question_list').length-1);
				// if (user_id === real_data.id) {
				// 	Student_My_Message(real_data.contents);
				// } else if (real_data.id === "lecture") {
				// 	Lecture_Message(real_data.contents);
				// } else {
				// 	Student_Other_Message(real_data.contents);
				// }
			});
		}
	});
}

var Lecture_Load_Chat = function() {
	$.ajax({
		type : "POST",
		url : "./Load_Chat.php",
		dataType : "json",
		data : {
			"room_code" : save_room_code
		},
		success : function(response) {
			console.log(response);
			$.each(response, function(index, real_data) {
				// if (real_data.id === "lecture") {
				// 	My_Message(real_data.contents);
				// } else {
				// 	Other_Message(real_data.contents);
				// }
				Other_Message(real_data.likes, real_data.contents);
				sorting(document.getElementsByClassName('question_list').length-1);
			});
		}
	});
}
var reset_student_data = function() {
	var student_data = document.getElementsByClassName("studentBox");
	$.ajax({
		type : "POST",
		url : "./Student_State.php",
		data : {
			"user_id" : "",
			"room_code" : save_room_code,
			"state" : 0
		},
		success : function(response) {
			for (var i = 0; i < student_data.length; i++) {
				student_data[i].style.backgroundColor = "#E6E6E6";
				student_data[i].getElementsByClassName("alertMark")[0].style.display = "none";
			}
		}
	});
}

var add_student_data = function(user_id, user_name, question, pass, state) {
	var addArea = document.getElementById("StudentList");
	var student_wrapper = document.createElement("div");
	var student_img = document.createElement("img");
	var nameContainer = document.createElement("p");
	var userName = document.createElement("span");
	var student_state = document.createElement("div");

	student_state.className = "student_state";
	student_wrapper.className = "student_box";
	student_img.className = "student_img";
	student_img.src = "../img/student.png";
	userName.className = "studentName";
	// Student's Information
	// if (state === "2") {
	// 	newElement.style.backgroundColor = "#59CC98";
	// 	imgContainer.src = "../img/check.png";
	// }
	// if (state === "1") {
	// 	newElement.style.backgroundColor = "#E85280";
	// 	imgContainer.src = "../img/exclamation.png";
	// }

	student_img.setAttribute("onclick", "openModal(\"" + user_name + "\")");
	// red: #E85280 , green: #59CC98

	// exclamation image = "../img/exclamation.png"
	userName.innerHTML = user_name;

	student_wrapper.id = user_id;
	nameContainer.appendChild(userName);
	student_wrapper.appendChild(student_img);
	student_wrapper.appendChild(nameContainer);
	student_wrapper.appendChild(student_state);
	addArea.appendChild(student_wrapper);
}

var complete_btn = function(student_id) {
	$.ajax({
		type : "POST",
		url : "./Student_State.php",
		data : {
			"user_id" : student_id,
			"room_code" : save_room_code,
			"state" : 2
		},
		success : function() {
			webSocket
					.send(JSON
							.stringify({
								to : "complete",
								user_id : student_id,
								room_code : document
										.getElementsByClassName("Room_Code")[is_lecture].innerHTML
							}));
			//alert("완료");
		}
	});
}

var question_btn = function(student_id) {
	$.ajax({
		type : "POST",
		url : "./Student_State.php",
		data : {
			"user_id" : student_id,
			"room_code" : save_room_code,
			"state" : 1
		},
		success : function() {
			webSocket
					.send(JSON
							.stringify({
								to : "question",
								user_id : student_id,
								room_code : document
										.getElementsByClassName("Room_Code")[is_lecture].innerHTML
							}));
			//alert("질문");
		}
	});
}

var send_link = function() {
	webSocket
			.send(JSON
					.stringify({
						to : "url",
						name : document.getElementById("send_name").innerHTML,
						url : document.getElementById("input_link").value,
						room_code : document
								.getElementsByClassName("Room_Code")[is_lecture].innerHTML
					}));
	document.getElementById("send_name").innerHTML = "";
	alert("전송 완료");
}

function Student_Enter() {
	var addArea = document.getElementById("StudentList");
	var newElement = document.createElement("div");
	var imgContainer = document.createElement("img");
	var nameContainer = document.createElement("p");
	var countNumberContainer = document.createElement("p");
	var countTextContainer = document.createElement("p");
	var userName = document.createElement("span");
	var questionNumber = document.createElement("span");
	var completedNumber = document.createElement("span");
	var questionText = document.createElement("span");
	var completedText = document.createElement("span");

	newElement.className = "studentBox";
	userName.className = "studentName";
	imgContainer.className = "alertMark";
	countNumberContainer.className = "countNumberContainer";
	questionNumber.className = "questionNumber";
	completedNumber.className = "completedNumber";
	questionText.className = "questionText";
	completedText.className = "completedText";
	// Student's Information
	newElement.style.backgroundColor = "#E6E6E6";
	// red: #E85280 , green: #59CC98
	imgContainer.src = "../img/check.png";
	// exclamation image = "../img/exclamation.png"
	userName.innerHTML = "최기현";
	questionText.innerHTML = "질문 수";
	questionNumber.innerHTML = "5";
	completedText.innerHTML = "통과 수";
	completedNumber.innerHTML = "4";

	newElement.id = createId(createdId_Index);
	createdId_Index++;

	countNumberContainer.appendChild(questionNumber);
	countNumberContainer.appendChild(completedNumber);
	countTextContainer.appendChild(questionText);
	countTextContainer.appendChild(completedText);
	nameContainer.appendChild(userName);
	nameContainer.appendChild(imgContainer);
	newElement.appendChild(nameContainer);
	newElement.appendChild(countNumberContainer);
	newElement.appendChild(countTextContainer);
	addArea.appendChild(newElement);
}

function createId(index) {
	var newID = "student" + index;
	return newID;
}

function slide_Down() {
	var showList = document.getElementById("greetings");
	showList.style.animationName = "slideDown";
	showList.style.animationDuration = "2s";
	showList.style.display = "table";
}

function slide_Up() {
	var showList = document.getElementById("SlideUp");
	showList.style.animationName = "slideUp";
	showList.style.animationDuration = "2s";
	showList.style.display = "table";
}

function EnterRoom() {
	var showList = document.getElementById("newSlideUp");
	var hideGreetings = document.getElementById("greetings");

	hideGreetings.style.animationName = "hideGreetings";
	hideGreetings.style.animationDuration = "2s";
	hideGreetings.style.display = "none";
	setTimeout(function() {
		hideGreetings.style.display = "none";
	}, 2000);

	showList.style.animationName = "bringNextPage";
	showList.style.animationDuration = "2s";
	showList.style.display = "table";
	document.getElementsByTagName("html")[0].style.overflow = "hidden";
}

function hidePage() {
	var hideList = document.getElementById("SlideUp");
	hideList.style.animationName = "secondSlideUp";
	hideList.style.animationDuration = "2s";
	setTimeout(function() { // 위로 올라가면 1초뒤에 완전히 보이지 안도록
		hideList.style.display = "none";
	}, 2000);
}

function bringLecturerPage() {
	var showList = document.getElementById("Lecturer");
	var hideGreetings = document.getElementById("greetings");

	hideGreetings.style.animationName = "hideGreetings";
	hideGreetings.style.animationDuration = "2s";
	hideGreetings.style.display = "none";
	setTimeout(function() {
		hideGreetings.style.display = "none";
	}, 2000);

	showList.style.animationName = "bringNextPage";
	showList.style.animationDuration = "2s";
	showList.style.display = "table";
	document.getElementsByTagName("html")[0].style.overflow = "hidden";
}

function get_out_StudentPage() {
	var showList = document.getElementById("newSlideUp");

	showList.style.animationName = "test2";
	showList.style.animationDuration = "2s";
	setTimeout(function() {
		showList.style.display = "none";
	}, 2000);
	setTimeout(function() {
		slide_Down();
	}, 500);
	document.getElementsByTagName("html")[0].style.overflow = "hidden";
	look_Page();
}

function get_out_LecturePage(){
	var showList = document.getElementById("Lecturer");

	showList.style.animationName = "test2";
	showList.style.animationDuration = "2s";
	setTimeout(function() {
		showList.style.display = "none";
	}, 2000);
	setTimeout(function() {
		slide_Down();
	}, 500);
	document.getElementsByTagName("html")[0].style.overflow = "hidden";
	look_Page();
}

function look_Page() {
	var hideList = document.getElementById("SlideUp");
	hideList.style.animationName = "test";
	hideList.style.animationDuration = "2s";
	hideList.style.display = "table";
}

function openModal(name) {
	document.getElementById("send_name").innerHTML = name;
	var showThis = document.getElementsByClassName("Modal")[0];
	var hide = document.getElementById("Lecturer");
	hide.style.opacity = "0.6";
	showThis.style.display = "block";
}

function closeModal() {
	var showThis = document.getElementsByClassName("Modal")[0];
	var hide = document.getElementById("Lecturer");
	hide.style.opacity = "1";
	showThis.style.display = "none";
}
var lecturer_chat_opened = 0;
$('#Open_Chat').click(function() {
	var chat_arrow = document.getElementById('chat_arrow');
	if(lecturer_chat_opened === 0){
		var hide = document.getElementById("Lecturer_contents");
		hide.style.filter = "brightness(60%)";
		hide.style.transition = "500ms";
		var Chat_Area = document.getElementById("lecturer_chat_container");
		Chat_Area.style.animationName = "bring_Lecturer_Chat";
		Chat_Area.style.animationDuration = "1000ms";
		Chat_Area.style.right = "0px";
		chat_arrow.style.transform = "scaleX(-1)";
		// Chat_Area.style.display = "block";
		lecturer_chat_opened = 1;
	}
	else{
		clearTimeout(time);
		var hide = document.getElementById("Lecturer_contents");
		hide.style.opacity = "1";
		hide.style.filter = "brightness(100%)";
		var Chat_Area = document.getElementById("lecturer_chat_container");
		Chat_Area.style.animationName = "hide_Lecturer_Chat";
		Chat_Area.style.animationDuration = "1000ms";
		Chat_Area.style.right = "-570px";
		chat_arrow.style.transform = "scaleX(1)";
		// setTimeout(function() {
		// 	Chat_Area.style.display = "none";
		// }, 1500);
		lecturer_chat_opened = 0;
	}
	disableSlide();
});

// $('#Close_Chat').click(function() {
// 	clearTimeout(time);
// 	$('#lecturer_chat_container').fadeOut('fast');
// 	disableSlide();
// 	var Chat_Area = document.getElementById("lecturer_chat_container");
// 	Chat_Area.style.animationName = "hide_Lecturer_Chat";
// 	Chat_Area.style.animationDuration = "2500ms";
// 	setTimeout(function() {
// 		Chat_Area.style.display = "none";
// 	}, 2500);
//
// });

function disableSlide() { // 이미지가 움직이는 동안 모든 slide버튼 비활성화
	var openButton = document.getElementById("Open_Chat");
	lockoutSubmit(openButton, 5500);
}
function lockoutSubmit(button, time) { // animation 겹침 현상 방지를 위해 버튼을 disable
	var oldValue = button.value;
	button.setAttribute('disabled', true); // 버튼 속성에 disable 추가
	setTimeout(function() {
		button.value = oldValue;
		button.removeAttribute('disabled'); // 애니메이션이 끝나면 disable 속성 제거
	}, time)
}

var Student_enter = function(event, student_id) {
	if (event.key == "Enter") {
		// send_btn(student_id);
		input_question(student_id);
	}
}

// var send_btn = function(student_id) {
// 	var text_value = document.getElementById("user_message");
// 	var room_code_data = document.getElementsByClassName("Room_Code")[is_lecture].innerHTML;
// 	if (text_value.value === "") {
// 		return;
// 	}
// 	$.ajax({
// 		type : "POST",
// 		url : "./Question.php",
// 		data : {
// 			"room_code" : room_code_data,
// 			"user_id" : student_id,
// 			"text" : text_value.value
// 		},
// 		success : function() {
// 			webSocket.send(JSON.stringify({
// 				to : "message",
// 				user_id : student_id,
// 				msg : text_value.value,
// 				room_code : room_code_data
// 			}));
// 			Student_My_Message(text_value.value);
// 			text_value.value = "";
// 			text_value.focus();
// 		}
// 	});
// }

var Lecture_enter = function(event) {
	if (event.key == "Enter") {
		lecture_send_btn();
	}
}

var lecture_send_btn = function() {
	var text_value = document.getElementById("myMessage");
	var room_code_data = document.getElementsByClassName("Room_Code")[is_lecture].innerHTML;
	if (text_value.value === "") {
		return;
	}
	$.ajax({
		type : "POST",
		url : "./Question.php",
		data : {
			"room_code" : room_code_data,
			"user_id" : "lecture",
			"text" : text_value.value
		},
		success : function() {
			webSocket.send(JSON.stringify({
				to : "message",
				user_id : "lecture",
				msg : text_value.value,
				room_code : room_code_data
			}));
			My_Message(text_value.value);
			text_value.value = "";
			text_value.focus();
		}
	});
}

function My_Message(text) {
	var message_Container = document.getElementById("Chat_Area");
	var message_Box = document.createElement("p");
	var message_Content = document.createElement("span");
	message_Content.className = "My_Message";
	// ********* change *********
	message_Content.innerHTML = text;

	message_Box.appendChild(message_Content);
	message_Container.appendChild(message_Box);
	message_Container.scrollTop = message_Container.scrollHeight;
}
function Other_Message(like,text) {
	var message_Container = document.getElementById("Chat_Area");
	var newElement = document.createElement("div");
  var textContainer = document.createElement("div");
  var question_number = document.createElement("input");
  var button_container = document.createElement("div");
  var delete_button = document.createElement("img");
  var like_button = document.createElement("img");
  var like_count = document.createElement("div");

	question_number.setAttribute("type", "hidden");
  question_number.value = document.getElementsByClassName('question_list').length + 1; //문제 번호
  like_count.innerHTML = like; //좋아요 갯수
  textContainer.innerHTML = text; //질문 내용

  delete_button.src = "../img/delete_question.png";
  delete_button.className = "delete_question";
	button_container.className = "button_container";
  like_count.className = "like_count";
	like_button.setAttribute("src", "../img/like_button.png");
  like_button.className = "like_button";
  textContainer.className = "text_container"
  newElement.className = "question_list";
  question_number.className = "question_number";
	like_button.setAttribute("onclick", "like_btn(this)");

  button_container.appendChild(like_button);
  button_container.appendChild(like_count);
	button_container.appendChild(delete_button);
	button_container.appendChild(question_number);
	newElement.appendChild(button_container);
  newElement.appendChild(textContainer);
	message_Container.appendChild(newElement);
	message_Container.scrollTop = message_Container.scrollHeight;
}

function Student_My_Message(text) {
	var message_Container = document.getElementById("ChatArea");
	var message_Box = document.createElement("p");
	var message_Content = document.createElement("span");
	message_Content.className = "My_Message";
	// ********* change *********
	message_Content.innerHTML = text;

	message_Box.appendChild(message_Content);
	message_Container.appendChild(message_Box);
	message_Container.scrollTop = message_Container.scrollHeight;
}

function Student_Other_Message(text) {
	var message_Container = document.getElementById("ChatArea");
	var message_Box = document.createElement("p");
	var message_Content = document.createElement("span");
	message_Content.className = "Other_Message";
	// ********* change *********
	message_Content.innerHTML = text;

	message_Box.appendChild(message_Content);
	message_Container.appendChild(message_Box);
	message_Container.scrollTop = message_Container.scrollHeight;
}

function Lecture_Message(text) {
	var message_Container = document.getElementById("ChatArea");
	var message_Box = document.createElement("p");
	var message_Content = document.createElement("span");
	message_Content.className = "Lecture_Message";
	// ********* change *********
	message_Content.innerHTML = text;

	message_Box.appendChild(message_Content);
	message_Container.appendChild(message_Box);
	message_Container.scrollTop = message_Container.scrollHeight;
}
var add_question = function(){
  var addArea = document.getElementById("questionContainer");
  var newElement = document.createElement("div");
  var textContainer = document.createElement("div");
  var question_number = document.createElement("div");
  var button_container = document.createElement("p");
  var delete_button = document.createElement("div");
  var like_button = document.createElement("img");
  var like_count = document.createElement("div");

  question_number.innerHTML = document.getElementsByClassName('question_list').length + 1; //문제 번호
  like_count.innerHTML = "3"; //좋아요 갯수
  textContainer.innerHTML = " Questions Here!! "; //질문 내용

  delete_button.innerHTML = "X";
  delete_button.className = "delete_question";
  like_count.className = "like_count";
  like_button.src = "../img/like_button.png";
  like_button.className = "like_button";
  textContainer.className = "text_container"
  newElement.className = "question_list";
  question_number.className = "question_number";

  button_container.appendChild(like_button);
  button_container.appendChild(like_count);
  newElement.appendChild(delete_button);
  newElement.appendChild(textContainer);
  newElement.appendChild(question_number);
  newElement.appendChild(button_container);
  addArea.appendChild(newElement);
}

var input_question = function(student_id){
	var input_data = document.getElementById("user_message");
	var room_code_data = document.getElementsByClassName("Room_Code")[is_lecture].innerHTML;
	if (input_data.value === "") {
		return;
	}
	$.ajax({
		type : "POST",
		url : "./Question.php",
		data : {
			"room_code" : room_code_data,
			"user_id" : student_id,
			"text" : input_data.value
		},
		success : function() {
			webSocket.send(JSON.stringify({
				to : "message",
				user_id : student_id,
				msg : input_data.value,
				room_code : room_code_data
			}));
			question("0", input_data.value);
			input_data.value = "";
			input_data.focus();
		}
	});
}
function question(like,text) {
	var message_Container = document.getElementById("questionContainer");
	var newElement = document.createElement("div");
  var textContainer = document.createElement("div");
  var question_number = document.createElement("input");
  var button_container = document.createElement("div");
  var delete_button = document.createElement("img");
  var like_button = document.createElement("img");
  var like_count = document.createElement("div");

	question_number.setAttribute("type", "hidden");
  question_number.value = document.getElementsByClassName('question_list').length + 1; //문제 번호
  like_count.innerHTML = like; //좋아요 갯수
  textContainer.innerHTML = text; //질문 내용

  delete_button.src = "../img/delete_question.png";
  delete_button.className = "delete_question";
	button_container.className = "button_container";
  like_count.className = "like_count";
	like_button.setAttribute("src", "../img/like_button.png");
  like_button.className = "like_button";
  textContainer.className = "text_container"
  newElement.className = "question_list";
  question_number.className = "question_number";
	like_button.setAttribute("onclick", "like_btn(this)");

  button_container.appendChild(like_button);
  button_container.appendChild(like_count);
	button_container.appendChild(delete_button);
	button_container.appendChild(question_number);
	newElement.appendChild(button_container);
  newElement.appendChild(textContainer);
	message_Container.appendChild(newElement);
	message_Container.scrollTop = message_Container.scrollHeight;
}

// var question = function(like, text){
//   var addArea = document.getElementById("questionContainer");
//   var newElement = document.createElement("div");
//   var textContainer = document.createElement("div");
//   var question_number = document.createElement("div");
//   var button_container = document.createElement("p");
//   var delete_button = document.createElement("div");
//   var like_button = document.createElement("img");
//   var like_count = document.createElement("div");
//
//   question_number.innerHTML = document.getElementsByClassName('question_list').length + 1; //문제 번호
//   like_count.innerHTML = like; //좋아요 갯수
//   textContainer.innerHTML = text; //질문 내용
//
//   delete_button.innerHTML = "X";
//   delete_button.className = "delete_question";
//   like_count.className = "like_count";
// 	like_button.setAttribute("src","../img/like_button.png");
//   like_button.className = "like_button";
// 	like_button.setAttribute("onclick", "like_btn(this)");
//
//   textContainer.className = "text_container"
//   newElement.className = "question_list";
//   question_number.className = "question_number";
//
//   button_container.appendChild(like_button);
//   button_container.appendChild(like_count);
//   newElement.appendChild(delete_button);
//   newElement.appendChild(textContainer);
//   newElement.appendChild(question_number);
//   newElement.appendChild(button_container);
//   addArea.appendChild(newElement);
// 	addArea.scrollTop = addArea.scrollHeight;
// }

var like_btn = function(mydata){
	var this_data = mydata.parentElement;
   var index_num = this_data.getElementsByTagName('input')[0].value;
   var user_id = document.getElementById("user_id").value;
   $.ajax({
      type : "POST",
      url : "./Question_like.php",
      data : {
         "room_code" : save_room_code,
         "user_id" : user_id,
         "index" : index_num
      },
      success : function(response) {
         response =  $.trim(response);
         if(response === 'duplicate'){
            alert("좋아요를 이미 클릭하였습니다.");
            return;
         }
         webSocket.send(JSON.stringify({
            to : "like",
            likes : response,
            like_index : $(this_data.parentElement).index(),
            room_code : save_room_code
         }));
         this_data.getElementsByTagName('div')[0].innerHTML = response;
         sorting($(this_data.parentElement).index());
      }
   });
}

var sorting = function(index){
	if(index == 0){
      return;
   }
   if(parseInt(document.getElementsByClassName("like_count")[index-1].innerHTML) < parseInt(document.getElementsByClassName("like_count")[index].innerHTML)){
      $(document.getElementsByClassName("question_list")[index-1]).insertAfter(document.getElementsByClassName("question_list")[index]);
      sorting(index-1);
   }
	 else if(parseInt(document.getElementsByClassName("like_count")[index-1].innerHTML) == parseInt(document.getElementsByClassName("like_count")[index].innerHTML)){
      if(parseInt(document.getElementsByClassName("question_number")[index-1].value) > parseInt(document.getElementsByClassName("question_number")[index].value)){
            $(document.getElementsByClassName("question_list")[index-1]).insertAfter(document.getElementsByClassName("question_list")[index]);
            sorting(index-1);
      }
   }
}
