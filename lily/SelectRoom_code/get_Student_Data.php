<?php
  include "../dbconnect.php";
  Header('Content-Type: application/json');
  $roomCode = $_POST['room_code'];
  $room_talbe = $roomCode . '_room';
  $sql = "SELECT * FROM $room_talbe";
  $res = $dbConnect->query($sql);
  $result_array = array();
  while ($row = $res->fetch_assoc()) {
    # code...
    $Student_array = array(
      "user_id" => $row['identity'],
      "user_name" => $row['name'],
      "question_count" => $row['question'],
      "pass_count" => $row['response'],
      "state" => $row['state']
    );
    array_push($result_array, $Student_array);
  }
  $myJSON = json_encode($result_array);
  echo $myJSON;
?>
