<?php
  include "../database_job/dbconnect.php";
  $roomcode = $_POST['roomcode'];

  $room_table = $roomcode . '_room';
  $sql = "SELECT * FROM `{$room_table}`";
  $res = $dbConnect->query($sql);
  echo "<table>";
  echo "<tr><td>아이디</td><td>이름</td><td>질문 수</td><td>통과 수</td><td>상태</td></tr>";
  while ($row = $res->fetch_assoc()) {
    # code...
    echo "<tr>";
    echo "<td>" . $row['identity'] . "</td>";
    echo "<td>" . $row['name'] . "</td>";
    echo "<td>" . $row['question'] . "</td>";
    echo "<td>" . $row['response'] . "</td>";
    echo "<td>" . $row['state'] . "</td>";
    echo "</tr>";
  }
  echo "</table>";
  echo "<hr>";

  $question_table = $roomcode . '_question';
  $sql = "SELECT * FROM `{$question_table}`";
  $res = $dbConnect->query($sql);
  echo "<table>";
  echo "<tr><td>번호</td><td>아이디</td><td>질문 내용</td><td>좋아요 수</td></tr>";
  while ($row = $res->fetch_assoc()) {
    # code...
    echo "<tr>";
    echo "<td>" . $row['index'] . "</td>";
    echo "<td>" . $row['identity'] . "</td>";
    echo "<td>" . $row['question'] . "</td>";
    echo "<td>" . $row['likes'] . "</td>";
    echo "</tr>";
  }
  echo "</table>";
  echo "<hr>";

  $like_table = $roomcode . '_like';
  $sql = "SELECT * FROM `{$like_table}`";
  $res = $dbConnect->query($sql);
  echo "<table>";
  echo "<tr><td>좋아요한 질문 번호</td><td>아이디</td></tr>";
  while ($row = $res->fetch_assoc()) {
    # code...
    echo "<tr>";
    echo "<td>" . $row['index'] . "</td>";
    echo "<td>" . $row['identity'] . "</td>";
    echo "</tr>";
  }
  echo "</table>";
  echo "<hr>";

?>
