var out_room = function(){
  var user_id = document.getElementById("user_id").value;
  $.ajax({
    type:"POST",
    url:"./out_room.php",
    data:{
      "user_id" : user_id
    },
    success:function(response){
      alert("나가기 성공");
    },error:function(response){
      console.log(response);
    }
  });
}

var remove_id = function(){
  var user_id = document.getElementById("user_id").value;
  $.ajax({
    type:"POST",
    url:"./remove_id.php",
    data:{
      "user_id" : user_id
    },
    success:function(response){
      alert("삭제 성공");
    },error:function(response){
      console.log(response);
    }
  });
}

var remove_room = function(){
  var roomcode = document.getElementById("room_code").value;
  $.ajax({
    type:"POST",
    url:"./remove_room.php",
    data:{
      "roomcode" : roomcode
    },
    success:function(response){
      alert("삭제 성공");
    },error:function(response){
      console.log(response);
    }
  });
}
