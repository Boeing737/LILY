var shadow_area = document.getElementById("shadow");

function fixedScrolled(e) {
    var evt = window.event || e;
    var delta = evt.detail ? evt.detail * (-120) : evt.wheelDelta; //delta returns +120 when wheel is scrolled up, -120 when scrolled down
    $("#StudentList").scrollTop($("#StudentList").scrollTop() - delta/5);
}

var mouse_wheel_event = (/Gecko\//i.test(navigator.userAgent)) ? "DOMMouseScroll" : "mousewheel";
if (shadow_area.attachEvent){
  shadow_area.attachEvent("on" + mouse_wheel_event, fixedScrolled);
}
else if(shadow_area.addEventListener){
  shadow_area.addEventListener(mouse_wheel_event, fixedScrolled, false);
}
