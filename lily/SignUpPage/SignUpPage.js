var SignUp_Form = document.SignUp_Form;
var duplicate_id = "";

var initialScreenSize = window.innerHeight;
window.addEventListener("resize", function() {
   if(window.innerHeight < initialScreenSize){
        $("#footer").hide();
   } else{
        $("#footer").show();
			}
});

var duplicate_check = function(){
	var SignUp_Id = SignUp_Form.SignUp_Id;

	if(SignUp_Id.value.length < 6){
		alert("6자 이상 입력하시오.");
		SignUp_Id.focus();
		return;
	}

	$.ajax({
		type:"POST",
		url:"./duplicate.php",
		data:{
      "SignUp_Id" : SignUp_Form.SignUp_Id.value
		},
		success:function(response){
      response = $.trim(response);
			if(response === "success"){
				alert("사용가능한 ID입니다.");
        duplicate_id = SignUp_Form.SignUp_Id.value;
				SignUp_Form.SignUp_Pw.focus();
			}else if(response === "fail"){
				alert("이미 존재하는 ID입니다.");
        SignUp_Form.SignUp_Id.value = "";
        SignUp_Form.SignUp_Id.focus();
			}
		}
	});
}

var onclick_Sign_Up = function(){
	if(SignUp_Form.SignUp_Name.value === ""){
		alert("이름을 작성하십시오.");
		SignUp_Form.SignUp_Name.focus();
		return;
	}else if(!is_duplicate_check()){
		return;
	}else if(!is_pw_equal()){
		return;
	}
	$.ajax({
  		type:"POST",
  		url:"./SignUp.php",
  		data:{
        "SignUp_Name" : SignUp_Form.SignUp_Name.value,
        "SignUp_Id" : SignUp_Form.SignUp_Id.value,
        "SignUp_Pw" : SignUp_Form.SignUp_Pw.value
  		},
  		success:function(response){
        response = $.trim(response);
  			if(response === "success"){
  				alert("회원가입이 완료되었습니다.");
					location.href='../index.html';
  			}else if(response === "fail"){
          alert("회원가입하는데 문제가 생겼습니다.");
        }
  		}
  	});
}

var is_duplicate_check = function(){
	if(duplicate_id === "" || duplicate_id !== SignUp_Form.SignUp_Id.value){
		alert("중복확인을 하십시오.");
		SignUp_Form.Duplicate_Check.focus();
		return false;
	}
	return true;
}

var is_pw_equal = function(){
	var SignUp_Pw = SignUp_Form.SignUp_Pw;
	var Pw_Check = SignUp_Form.Pw_Check;
	if(SignUp_Pw.value.length < 8){
		alert("비밀번호를 8자 이상 하십시오.");
		Pw_Check.value = "";
		SignUp_Pw.focus();
		return false;
	}else if(!ischar(SignUp_Pw.value)){
		alert("문자를 포함하시오.");
		Pw_Check.value = "";
		SignUp_Pw.focus();
		return false;
	}else if(!isnum(SignUp_Pw.value)){
		alert("숫자를 포함하시오.");
		Pw_Check.value = "";
		SignUp_Pw.focus();
		return false;
	}else if(SignUp_Pw.value !== Pw_Check.value){
		alert("비밀번호를 확인하시오.");
		Pw_Check.value = "";
		Pw_Check.focus();
		return false;
	}
	return true;
}

//숫자 포함 확인
var isnum = function(data){
  var num = /[0-9]/gi;
  return num.test(data);
}

//특수문자 확인 제외
var isspecial_pattern = function(data){
  var special_pattern = /[`~!@#$%^&*|\\\'\";:\/?]/gi;
  return special_pattern.test(data);
}

//문자 포함 확인
var ischar = function(data){
  var chardata = /[a-zA-Z]/gi;
  return chardata.test(data);
}
