var initialScreenSize = window.innerHeight;
window.addEventListener("resize", function() {
   if(window.innerHeight < initialScreenSize){
        $("#footer").hide();
   } else{
        $("#footer").show();
			}
});


var onclick_login_btn = function(){
	var Login_Form = document.Login_Form;
	var user_Id = Login_Form.user_Id;
	var user_Pw = Login_Form.user_Pw;

	if(user_Id.value === "" || user_Pw.value === ""){
		alert("아이디 또는 비밀번호를 입력하시오.");
		return;
	}

	$.ajax({
		type:"POST",
		url:"./MainPage/MainPage.php",
		dataType : "json",
		data:{
			"Login_Id" : user_Id.value,
			"Login_Pw" : user_Pw.value,
		},
		success:function(response){
			console.log(response.correct);
			if(response.correct === 'correct'){
				Login_Form.user_Name.value = response.name;
        Login_Form.submit();
			}else{
				alert("아이디 또는 비밀번호를 확인하시오.");
        user_Pw.value = "";
        user_Id.value = "";
        user_Id.focus();
			}
		}
	});
}

var key_press_enter = function(event){
	if(event.key == "Enter"){
		onclick_login_btn();
	}
}
