var webSocket = new WebSocket('ws://13.124.150.80:8080/LILY/WebSocket');
// var save_json_data = [{"name":"asdf1","id":"asdf1","pw":"ass1"},{"name":"asdf2","id":"asdf2","pw":"ass2"},{"name":"asdf3","id":"asdf3","pw":"ass3"},{"name":"asdf4","id":"asdf4","pw":"ass4"},{"name":"asdf5","id":"asdf5","pw":"ass5"},{"name":"asdf6","id":"asdf6","pw":"ass6"},{"name":"asdf7","id":"asdf7","pw":"ass7"},{"name":"asdf8","id":"asdf8","pw":"ass8"},{"name":"asdf9","id":"asdf9","pw":"ass9"},{"name":"asdf10","id":"asdf10","pw":"ass10"}];
var save_json_data;
var make_user = function(){
  var user_name = document.getElementById("make_user_name").value;
  var user_id = document.getElementById("make_user_id").value;
  var user_pw = document.getElementById("make_user_pw").value;
  var user_num = document.getElementById("make_user_num").value;

  $.ajax({
		type:"POST",
		url:"./Make_User.php",
    dataType : "json",
		data:{
      "user_name" : user_name,
      "user_id" : user_id,
      "user_pw" : user_pw,
      "user_num" : user_num
		},
		success:function(response){
      save_json_data = response;
      console.log(response);
		},error:function(response){
      console.log(response);
    }
	});
}

var enter_room = function(){
  var room_num = document.getElementById("room_num").value;
  alert(room_num);
  $.each(save_json_data, function(index, real_data) {
    $.ajax({
  				type : "POST",
  				url : "../SelectRoom/EnterRoom.php",
  				data : {
  					"room_code" : room_num,
  					"user_id" : real_data.id
  				},
  				success : function(data) {
            console.log(real_data.name + real_data.id + real_data.pw);
            get_Student_Count(real_data.id, real_data.name, room_num, "0");
  				}
  			});
  });
}

var get_Student_Count = function(user_id, user_name, room_code, lecture) {
  $.each(save_json_data, function(index, real_data) {
  	$.ajax({
  				type : "POST",
  				url : "../SelectRoom/get_Student_Count.php",
  				dataType : "json",
  				data : {
  					"room_code" : room_code,
  					"user_id" : user_id
  				},
  				success : function(response) {
  					console.log(response);
  					webSocket.send(JSON.stringify({
  						to : "people",
  						Student_Count : response.Student_Count,
  						state : response.state,
  						user_id : user_id,
  						user_name : user_name,
  						room_code : room_code
  					}));
  				}
  		});
  });
}

var out_room = function() {
  var room_num = document.getElementById("out_room_num").value;
  $.each(save_json_data, function(index, real_data) {
	   $.ajax({
				type : "POST",
				url : "../SelectRoom/get_out_Student.php",
				dataType : "json",
				data : {
					"room_code" : room_num,
					"user_id" : real_data.id
				},
				success : function(response) {
					console.log(response);
               response = $.trim(response);
               webSocket.send(JSON.stringify({
                  to : "out",
                  Student_Count : response,
                  user_id : real_data.id,
                  room_code : room_num
               }));
				}
			});
    });
}

var remove_user = function(){
  $.each(save_json_data, function(index, real_data) {
    $.ajax({
  		type:"POST",
  		url:"./Remove_User.php",
  		data:{
        "user_id" : real_data.id
  		},
  		success:function(response){
        console.log(response);
  		},error:function(response){
        console.log(response);
      }
  	});
  });
}
