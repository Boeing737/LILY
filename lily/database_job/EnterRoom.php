<?php
  include "dbconnect.php";
  $memberId = $_POST['user_id'];
  $roomCode = $_POST['room_code'];

  $sql = "SELECT * FROM roomdata WHERE roomcode='{$roomCode}'";
  $row = $dbConnect->query($sql);
  //방 존재하지 않을 때
  if($row->num_rows < 1){
    echo "ERROR";
    exit;
  }

  $sql = "SELECT * FROM userdata WHERE identity='{$memberId}'";
  $res = $dbConnect->query($sql);
  $row = $res->fetch_assoc();

  if($row['roomdata'] == 0){
    $sql = "UPDATE userdata SET roomdata='{$roomCode}' WHERE identity='{$memberId}'";
    $memberName = $row['name'];
    if($dbConnect->query($sql)){
      //room table에 입장한 사람 정보 저장
      $room_table = $roomCode . '_room';
      $sql = "INSERT INTO `{$room_table}` VALUES('{$memberId}', '{$memberName}', '-1', '0', '0')";
      $dbConnect->query($sql);

      //해당 방 인원 증가
      $sql = "UPDATE roomdata SET num=num+1 WHERE roomcode='{$roomCode}'";
      $dbConnect->query($sql);

      echo "NOT_ERROR";
    }
    else{
      echo 'ERROR';
    }
  }else if($row['roomdata'] == $roomCode){
    //기존 입장 시 아무 처리 없음
    $sql = "SELECT * FROM roomdata WHERE roomcode='{$roomCode}'";
    $res = $dbConnect->query($sql);
    $room_row = $res->fetch_assoc();
    if(!strcmp($room_row['lecturer'], $memberId)){
      echo "LECTURE";
    }else{
      echo "NOT_ERROR";
    }
  }else{
    //다른방에 입장 중
    echo $row['roomdata'];
  }
?>
