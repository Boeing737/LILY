<?php
  include "dbconnect.php";
  Header('Content-Type: application/json');
  $roomCode = $_POST['room_code'];
  $question_table = $roomCode . '_question';
  $sql = "SELECT * FROM $question_table";
  $res = $dbConnect->query($sql);
  $result_array = array();
  while ($row = $res->fetch_assoc()) {
    # code...
    $Student_array = array(
      "id" => $row['identity'],
      // "user_name" => ,
      "contents" => $row['question'],
      "likes"=> $row['likes']
    );
    array_push($result_array, $Student_array);
  }
  $myJSON = json_encode($result_array);
  echo $myJSON;
?>
