<?php
  include "dbconnect.php";
  $memberId = $_POST['user_id'];
  $randomNum = mt_rand(1000, 9999);

  $sql = "SELECT * FROM userdata WHERE identity='{$memberId}'";
  $res = $dbConnect->query($sql);
  $row = $res->fetch_assoc();

  //방 생성자가 기존 입장한 방이 있는지 확인
  if($row['roomdata'] != 0){
    $myObj = array(
      "error" =>'exist',
      "roomcode" => $row['roomdata']
    );
    $myJSON = json_encode($myObj);

    echo $myJSON;
    exit;
  }

  while (true) {
    $sql = "SELECT * FROM roomdata WHERE roomcode='{$randomNum}'";
    $row = $dbConnect->query($sql);
    //랜덤을 통한 방이 존재하지 않을 때
    if($row->num_rows < 1){
      //방 정보 생성
      $sql = "INSERT INTO roomdata VALUES('{$randomNum}', '1', '{$memberId}')";
      if($dbConnect->query($sql)){
        //사용자 입장 방 설정
        $sql = "UPDATE userdata SET roomdata='{$randomNum}' WHERE identity='{$memberId}'";
        if($dbConnect->query($sql)){
          $myObj = array(
            "error" =>'success',
            "roomcode" => $randomNum
          );
          //방 정보 테이블 생성
          $room_table = $randomNum . '_room';
          $sql = "CREATE TABLE `{$room_table}`(`identity` char(16) NOT NULL , `name` char(16) NOT NULL, `question` tinyint NOT NULL , `response`  tinyint NOT NULL , `state` enum('0', '1', '2') NOT NULL, PRIMARY KEY (`identity`));";
          $dbConnect->query($sql);
          //질문 정보 테이블 생성
          $question_table = $randomNum . '_question';
          $sql = "CREATE TABLE `{$question_table}`(`index` tinyint NOT NULL AUTO_INCREMENT PRIMARY KEY, `identity` char(16) NOT NULL, `question` text NOT NULL, `likes` tinyint NOT NULL);";
          $dbConnect->query($sql);
          // 좋아요 정보 테이블 생성
          $like_table = $randomNum . '_like';
          $sql = "CREATE TABLE `{$like_table}`(`index` tinyint NOT NULL, `identity` char(16) NOT NULL);";
          $dbConnect->query($sql);
        }
        else{
          $myObj = array(
            "error" =>'error',
            "roomcode" => $randomNum
          );
        }
      }else{
        $myObj = array(
            "error" =>'error',
            "roomcode" => $randomNum
          );
      }
      break;
    }
  }

  $myJSON = json_encode($myObj);

  echo $myJSON;
?>
