var eduPage_wrapper = document.getElementsByClassName('eduPage_wrapper')[0];
var body = document.getElementsByTagName('body')[0];

function openEduPage(){
  body.style.overflow = 'hidden';
  //mainSection.style.filter = 'brightness(50%)';
  eduPage_wrapper.style.display = 'block';
}

// bind("mouseup touchend") = 모바일 터치 이벤트도 마우스 클릭 이벤트처럼 인식되도록 설정
$(document).bind("mouseup touchend", function(e)
{
    var container = $('.eduPage');

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0)
    {
        //container.hide();
        eduPage_wrapper.style.display = 'none';
        body.style.overflow = 'scroll';
    }
});
